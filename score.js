const balls = [];

function onScoreUpdate(dropPosition, bounciness, size, bucketLabel) {
  balls.push([dropPosition, bounciness, size, bucketLabel]);
}

function runAnalysis() {
  const testSize = 100,
    [test, training] = splitData(balls, testSize);
  let score = 0;
  
  _.range(1,20).forEach(k => {
    const accurancy = _.chain(test)
    .filter(testPoint => knn(training, testPoint[0], k) === testPoint[3])
    .size()
    .divide(testSize)
    .value()
  
    console.log( `For k=${k} accurancy is ${accurancy}`);
  });

}

function calculateDistance(pointA, pointB) {
  return Math.abs(pointA - pointB);
}

function knn(data, point, k) {
  return _.chain(data)
  .map(row => [calculateDistance(row[0], point), row[3]])
  .sortBy(row => row[0])
  .slice(0, k)
  .countBy(row => row[1])
  .toPairs()
  .sortBy(row => row[1]).last().first().toNumber().value();
}

function splitData(data, testSetLength) {
  const shuffled = _.shuffle(data);
 
  const test = _.slice(shuffled, 0, testSetLength);
  const training = _.slice(shuffled, testSetLength);
  return [test, training];
}
